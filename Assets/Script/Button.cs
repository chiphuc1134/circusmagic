﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	public 			Rect 			rect;
	public 			Texture2D 		texture;

	bool justClick;
	bool isDown;
	bool disible;

	public Button(Texture2D texture)
	{
		this.texture = texture;
	}

	public Button(Texture2D texture, Rect rect)
	{
		this.texture = texture;
		this.rect = rect;
	}

	public void Disible()
	{
		disible = !disible;
	}

	void Update()		
	{
		if (disible)
			return;
		
		if(Application.platform == RuntimePlatform.Android)
		{	
			foreach (Touch touch in Input.touches ) {
				
				if(touch.phase == TouchPhase.Began){
					justClick = false;
					if(rect.Contains(new Vector2(touch.position.x, Screen.height - touch.position.y))){
						if(!isDown)
						{
							isDown = true;
						}
					}else{
						isDown = false;
					}
				}
				
				if(touch.phase == TouchPhase.Ended){
					if(isDown && rect.Contains(new Vector2(touch.position.x, Screen.height - touch.position.y))){
						justClick = true;
					}			
					else
					{
						justClick = false;
					}
					isDown = false;
				}
				
			}
			
		}
		
		if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
		{
			if(Input.GetMouseButtonDown(0))
			{
				justClick = false;
				Vector3 mousePos = Input.mousePosition;				
				
				
				if(rect.Contains(new Vector2(mousePos.x, Screen.height - mousePos.y)))
				{
					if(!isDown)
					{
						isDown = true;
					}
				}else{
					isDown = false;
				}
			}
			
			if(Input.GetMouseButtonUp (0))
			{
				Vector3 mousePos = Input.mousePosition;				
				if(isDown && rect.Contains(new Vector2(mousePos.x, Screen.height - mousePos.y)))
				{
					justClick = true;
				}			
				else
				{
					justClick = false;
				}
				isDown = false;
			}		
		}			
	}	

	public bool IsDown()
	{
		Update ();

		return isDown;
	}
	
	public bool isJustClick()
	{		
		Update ();

		if(justClick)
		{
			justClick = false;			
			return true;
		}
		
		return false;
	}

	public void Draw()
	{
		if(isDown && !disible)
		{
			Rect r = new Rect(rect);
			float k = 3;
			r.x += k;
			r.y += k;
			r.width -= 2 * k;
			r.height -= 2 * k;
			GUI.DrawTexture(r, texture, ScaleMode.StretchToFill);
		}
		else
		{
			GUI.DrawTexture(rect, texture, ScaleMode.StretchToFill);
		}
	}

	public void Draw(Rect rectGroup, Rect rect)
	{
		this.rect = new Rect(rectGroup.x + rect.x, rectGroup.y + rect.y, rect.width, rect.height);
		
		if(isDown && !disible)
		{
			Rect r = new Rect(rect);
			float k = 3;
			r.x += k;
			r.y += k;
			r.width -= 2 * k;
			r.height -= 2 * k;
			GUI.DrawTexture(r, texture, ScaleMode.StretchToFill);
		}
		else
		{
			GUI.DrawTexture(rect, texture, ScaleMode.StretchToFill);
		}
	}
}
