﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO; 

public class Canon : MonoBehaviour {
//	Vector2 startPos;

	public 			GameObject 			player;
	public 			GameObject 			bullet;
	public 			GameObject 			dot;

	public			SceneStage	 		MainScene;

	private			GameObject[] 		ind;
	private			int 				dots = 70;

	private			float 				power = 0;

	private			float 				angle;

	private			Vector2				startPosition;
	private			bool				aiming;

	void Start () {	
		ind = new GameObject[dots];
		for(int i = 0; i<dots; i++){
			GameObject d = (GameObject)Instantiate(dot);
			dot.renderer.enabled = false;
			ind[i] = d;
		}
	}	

	void Update () {
		if (aiming) {
			GetAngle ();
			
			CalculatePath ();
		}

		if(Application.platform == RuntimePlatform.Android){
			ActionTouch ();
		}else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			ActionKey();
		}		
	}


	#region Action
	void ActionKey(){
//		if (Input.GetKeyDown ("a")) {
//			for (int i = 1; i < dots; i++) {
//
//				System.IO.File.AppendAllText("C:/somewhere.txt", "\r\n{" + ind [i].transform.position.x + "f, " + ind [i].transform.position.y + "f},");
//			}
//		}

		if (Input.GetMouseButtonDown (0)) {
			Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			if(collider2D.OverlapPoint(touchPos)){
				aiming = true;

				MainScene.ChangeState(Define.STATE_SCENE_AIMING);

				startPosition = Input.mousePosition;
			}
		}

		if (Input.GetMouseButtonUp (0)) {
			if(aiming){
				aiming = false;

				MainScene.ChangeState(Define.STATE_SCENE_IDLE);

				player.GetComponent<Player>().Shoot(GetForce());

				HidePath();
			}
		}

	}
	
	void ActionTouch(){
		if(Input.GetTouch(0).phase == TouchPhase.Began){
			Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			if(collider2D.OverlapPoint(touchPos)){
				aiming = true;
				
				MainScene.ChangeState(Define.STATE_SCENE_AIMING);
				
				startPosition = Input.mousePosition;
			}
		}

		if(Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			if(aiming){
				aiming = false;
				
				MainScene.ChangeState(Define.STATE_SCENE_IDLE);
				
				player.GetComponent<Player>().Shoot(GetForce());
				
				HidePath();
			}
		}
	}
	#endregion

	void GetAngle (){
		Vector3 lookPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		
		float dx = lookPos.x - transform.position.x;
		float dy = lookPos.y - transform.position.y;
		
		angle = (Mathf.Atan2 (dy, dx) * Mathf.Rad2Deg) + 180; 
		
		transform.eulerAngles = new Vector3 (0, 0, angle);
	}
	
	Vector2 GetForce(){

		Vector3 lookPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		float stepForce = Mathf.Clamp(Vector2.Distance (transform.position,lookPos ),0,2f);


		power = 700/2f * stepForce;

		Vector2 v1 = new Vector2 (0,0);
		
		Vector2 v2 = GetVector(angle);
		
		Vector2 vSum = (v1 + v2);

		//		print("v1: " + v1 +" + "+ "v2: " + v2 + "v1 - v2 = " + vSum.normalized);
		return vSum.normalized * power;

	}

	Vector2 GetVector(float angle){
		float a = Mathf.Cos(angle* Mathf.Deg2Rad) * 10;
		float b = Mathf.Sin (angle* Mathf.Deg2Rad) * 10;
		
		return new Vector2 (a, b);
	}

	void CalculatePath(){

		Vector2 vel = GetForce(); //get velocity
		
		for(int i = 1; i < dots; i++){          
			ind[i].renderer.enabled = true; //make them visible
			Vector3 point = PathPoint(transform.position, vel, i); //get position of the dot 
			point.z = -1.0f;
			ind[i].transform.position = point;
		} 
	}

	float interval = 1/60.0f; 
	Vector2 PathPoint(Vector2 startP, Vector2 startVel, int n){
		//Standard formula for trajectory prediction
		float t = interval;
		Vector2 stepVelocity = t*startVel;

		Vector2 StepGravity = t* new Vector2(0,-9.8f);

		Vector2 whattoreturn = (startP + ((n * stepVelocity)+(n*n+n)*StepGravity) * 0.0485f);
		
		return whattoreturn;
	}

	void HidePath(){
		for (int i = 1; i < dots; i++) {          
			ind [i].renderer.enabled = false; //make them visible
		}
	}
}
