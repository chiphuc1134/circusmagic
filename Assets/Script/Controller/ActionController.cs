using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionController : MonoBehaviour {

	public	 		List<IActionPhuc> 	actions = new List<IActionPhuc>();
	private 		int 				kindOfAction;

	void Start () {
	}
	
	public void Update () {
		if (kindOfAction == Define.ACTION_KIND_SEQUENCE) {
			foreach(IActionPhuc action in actions){
				if(!action.getActionDone())
					action.Update();
			}
		}
	}

	public void add(IActionPhuc action){
		actions.Add (action);
	}

	public void stopAllAction(){
		for (int i = (actions.Count-1); i >=0; i--) {
			actions.Remove(actions[i]);
		}
	}

	public bool allActionsActive(){
		int actionCount = 0;
		foreach(IActionPhuc action in actions){
			if(action.getActionDone())
				actionCount++;
		}

		if (actionCount == actions.Count)
			return false;
		else
			return true;
	}
}
