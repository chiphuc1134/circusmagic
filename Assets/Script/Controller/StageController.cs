using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageController : MonoBehaviour {

	public	 		List<IActionPhuc> 	stages = new List<IActionPhuc>();
	private 		int 				kindOfAction;

	void Start () {
	}
	
	public void Update () {
		if (kindOfAction == Define.ACTION_KIND_SEQUENCE) {
			foreach(IActionPhuc stage in stages){
				stage.Update();

				if(stages.Count == 0)
					return;
			}
		}
	}

	public void Draw () {
		if (kindOfAction == Define.ACTION_KIND_SEQUENCE) {
			foreach(IActionPhuc stage in stages){
				stage.Draw();

				if(stages.Count == 0)
					return;
			}
		}
	}

	public void add(IActionPhuc stage){
		stages.Add (stage);
	}

	public void stopAllStage(){
		for (int i = (stages.Count-1); i >=0; i--) {
			stages.Remove(stages[i]);
		}
	}
}
