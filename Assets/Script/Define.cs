using UnityEngine;
using System.Collections;


public class Define {

	public static float WIDTH = 800;
	public static float HEIGHT = 480;

	public static float PERCENT_SCORE = 0;

	public static int CUR_LEVEL = 1;

	public static int ACTION_KIND_SEQUENCE = 0;
	public static int ACTION_KIND_SPAWN = 1;

	public const int DIR_LEFT = 1;
	public const int DIR_RIGHT = 2;

	public const int STATE_CHAR_BEGIN = 0;
	public const int STATE_CHAR_JUMP = 1;
	public const int STATE_CHAR_IN_THE_GROUND = 2;
	public const int STATE_CHAR_IN_MYSTERY_BOX = 3;
	public const int STATE_CHAR_SHOOT_BY_MYSTERY_BOX = 4;
	public const int STATE_CHAR_JUMP_BY_MYSTERY_BOX = 5;

	public const int STATE_MENU = 0;
	public const int STATE_MENU_CHOOSE_LEVEL = 1;


	public const int ANIM_IDLE = 0;
	public const int ANIM_JUMP = 1;	
	public const int ANIM_ROTATING = 2;
	public const int ANIM_ROTATING_BALL = 3;

	public const int STATE_SCENE_IDLE = 0;
	public const int STATE_SCENE_PAUSE = 1;
	public const int STATE_SCENE_AIMING = 2;
	public const int STATE_SCENE_END = 3;
	public const int STATE_SCENE_CHECK_RESULT = 4;
}
