﻿using UnityEngine;
using System.Collections;

public class FiringRing : MonoBehaviour {

	public			GameObject			combo;
	void Start () {
		combo = gameObject.transform.FindChild ("Combo").gameObject;

		combo.renderer.enabled = true;

		Invoke ("Destroy", 0.5f);
	}	

	void Update () {
		combo.transform.position += new Vector3 (0, 0.2f*Time.deltaTime);
	}

	void Destroy(){
		combo.renderer.enabled = false;
	}
}
