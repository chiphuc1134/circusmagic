﻿using UnityEngine;
using System.Collections;

public class IActionPhuc : MonoBehaviour{
	protected 	 		GameObject 			player;
	protected 	 		bool				actionDone;

	public virtual void Update(){}

	public virtual void Draw(){}

	protected virtual void End(){}

	public bool getActionDone(){
		return actionDone;
	}
}
