﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	private			GameObject 			target;
	private			Player	 			player;

	private			SceneStage 			mainScene;


	public			bool				canMoveUpdatePosition;					
	public			bool				moveLeft;

	private	 		Vector3 			startPos;
	private	 		Vector3 			curPosCamera;

	private	 		int 				orthographicSizeMin = 5;
	private	 		int 				orthographicSizeMax = 10;
	
	void Awake(){
		target = GameObject.FindWithTag ("Player");

		player = target.GetComponent<Player> ();

		mainScene = GameObject.Find ("SceneStage").GetComponent<SceneStage>();
	}

	void Start () {


	}
	

	void Update () {
		if(Application.platform == RuntimePlatform.Android){
			ActionTouch ();
		}else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			ActionKey();
		}		

		if (player.STATE == Define.STATE_CHAR_JUMP) {
			if (target.transform.position.y > 4)
				camera.orthographicSize = target.transform.position.y + 1; 
			
			transform.position = new Vector3 (target.transform.position.x + 4, transform.position.y, -10);
			
		} else if (mainScene.STATE == Define.STATE_SCENE_END) {
			Vector3 desPos = player.transform.position + new Vector3 (0, 0, -10);
			
			transform.position = Vector3.Lerp (transform.position, desPos, 3 * Time.deltaTime);
			camera.orthographicSize = Mathf.Lerp (camera.orthographicSize, 3, 7 * Time.deltaTime);
		} else if (player.STATE == Define.STATE_CHAR_IN_MYSTERY_BOX || player.STATE == Define.STATE_CHAR_SHOOT_BY_MYSTERY_BOX) {
			Vector3 desPos = player.transform.position + new Vector3 (0, 0, -10);

			transform.position = Vector3.Lerp (transform.position, desPos, 3 * Time.deltaTime);
			camera.orthographicSize = Mathf.Lerp (camera.orthographicSize, 3, 7 * Time.deltaTime);
		}else if (player.STATE == Define.STATE_CHAR_JUMP_BY_MYSTERY_BOX) {
			Vector3 desPos = player.transform.position + new Vector3 (0, 0, -10);
			
			transform.position = Vector3.Lerp (transform.position, desPos, 3 * Time.deltaTime);
			camera.orthographicSize = Mathf.Lerp (camera.orthographicSize, 5, 7 * Time.deltaTime);
		}
	}

	#region Action
	void ActionKey(){
		if (mainScene.STATE == Define.STATE_SCENE_IDLE) {
			
			
			if (Input.GetMouseButtonDown (0)) {
				startPos = Input.mousePosition;
				
				curPosCamera = camera.transform.position;		
				
			}
			
			if (Input.GetMouseButtonUp (0)) {
				if (camera.transform.position.x <= -2)
					camera.transform.position = Vector3.Lerp (camera.transform.position, new Vector3 (0, 0, -10), 5 * Time.deltaTime);
				else if (camera.transform.position.x >= 13.7f)
					camera.transform.position = Vector3.Lerp (camera.transform.position, new Vector3 (12, 0, -10), 5 * Time.deltaTime);
			}
			
			//move camera left right
			if (Input.GetMouseButton (0)) {
				Vector3 curPos = Input.mousePosition;
				
				float posX = startPos.x - curPos.x;
				
				camera.transform.position = curPosCamera + new Vector3 (posX * 0.05f, 0, -10);
				
				camera.transform.position = new Vector3 (Mathf.Clamp (camera.transform.position.x, -2, 13.7f), camera.transform.position.y, camera.transform.position.z);
			}


			
			if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
			{
				camera.orthographicSize++;
			}
			if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
			{
				camera.orthographicSize--;
			}
			
			camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, orthographicSizeMin, orthographicSizeMax );
		}
	}
	
	//buckets for caching our touch positions
	private Vector2 currTouch1 = Vector2.zero,
	lastTouch1 = Vector2.zero,
	currTouch2 = Vector2.zero,
	lastTouch2 = Vector2.zero;

	//used for holding our distances and calculating our zoomFactor
	private float currDist = 0.0f,
	lastDist = 0.0f,
	zoomFactor = 0.0f;

	public float zoomSpeed = 5.0f;

	void ActionTouch(){
		int fingerCount = 0;
		foreach (Touch touch in Input.touches) {
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
				fingerCount++;
		}

		if (fingerCount > 1) {
			if (Input.GetTouch (0).phase == TouchPhase.Moved && Input.GetTouch (1).phase == TouchPhase.Moved) {
				currTouch1 = Input.GetTouch(0).position;

				if(lastTouch1 == Vector2.zero)
					lastTouch1 = Input.GetTouch(0).position;
				
				currTouch2 = Input.GetTouch(1).position;

				if(lastTouch2 == Vector2.zero)
					lastTouch2 = Input.GetTouch(1).position;

				currDist = Vector2.Distance(currTouch1, currTouch2);
				lastDist = Vector2.Distance(lastTouch1, lastTouch2);

			}

			//Calculate the zoom magnitude
			zoomFactor = (lastDist - currDist)* Time.deltaTime;

//			print("currDist: " + currDist + " " + "lastDist: " + lastDist + "zoomFactor: " + zoomFactor );

			//apply zoom to our camera
			camera.orthographicSize = Mathf.Clamp(camera.orthographicSize -  zoomFactor, orthographicSizeMin, orthographicSizeMax );

//			Camera.mainCamera.transform.Translate(Vector3.forward * zoomFactor * zoomSpeed * Time.deltaTime);
		}


		if (mainScene.STATE == Define.STATE_SCENE_IDLE) {
			
			
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
				startPos = Input.mousePosition;
				
				curPosCamera = camera.transform.position;		
				
			}
			
			if (Input.GetTouch (0).phase == TouchPhase.Ended) {
				if (camera.transform.position.x <= -2)
					camera.transform.position = Vector3.Lerp (camera.transform.position, new Vector3 (0, 0, -10), 5 * Time.deltaTime);
				else if (camera.transform.position.x >= 13.7f)
					camera.transform.position = Vector3.Lerp (camera.transform.position, new Vector3 (12, 0, -10), 5 * Time.deltaTime);
				
				lastTouch1 = lastTouch2 = Vector2.zero;
			}
			
			if (Input.GetTouch (0).phase == TouchPhase.Moved) {
				Vector3 curPos = Input.mousePosition;
				
				float posX = startPos.x - curPos.x;
				
				camera.transform.position = curPosCamera + new Vector3 (posX * 0.05f, 0, -10);
				
				camera.transform.position = new Vector3 (Mathf.Clamp (camera.transform.position.x, -2, 13.7f), camera.transform.position.y, camera.transform.position.z);
			}
		}
	}


	#endregion
}
