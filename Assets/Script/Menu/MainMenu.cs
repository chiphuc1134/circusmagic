﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	private			StageController stageControler;
	public	 		int					STATE;
	public	 		string	 			pState;
	void Awake(){
				Screen.SetResolution (800, 480, false);
		}

	void Start () {
		stageControler = new StageController();

		ChangeState (Define.STATE_MENU_CHOOSE_LEVEL);
	}
	
	void Update () {
		stageControler.Update ();
	}

	void OnGUI () {
		stageControler.Draw ();
	}

	#region ChangeState
	public void ChangeState(int state){
		stageControler.stopAllStage ();
		
		switch (state) {
			
		case Define.STATE_MENU_CHOOSE_LEVEL:
			stageControler.add(new Menu_Stage_ChooseLevel());	
			
			pState = "STATE_MENU_CHOOSE_LEVEL";
			
			break;
		
			
		}	
		
		STATE = state;		
	}
	
	
	#endregion
}
