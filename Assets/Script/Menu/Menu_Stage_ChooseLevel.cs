﻿using UnityEngine;
using System.Collections;

public class Menu_Stage_ChooseLevel : IActionPhuc {

	private 			int 			selGridInt = -1;
	private 			string[] 		selStrings;

	public Menu_Stage_ChooseLevel(){

	}

	public override void Update () {
		selStrings = new string[4];

		for (int i = 0; i < 4; i++) {
			selStrings[i] = "Level " + (i+1);
		}
	}

	public override void Draw () {
		GUILayout.BeginArea (new Rect(75, 50, Define.WIDTH - 75*2, Define.HEIGHT - 50*2));

		//set suistyle default of the skin
		GUI.skin.button.margin.left = 75;
		GUI.skin.button.margin.right = 75;
		GUI.skin.button.margin.top = 50;
		GUI.skin.button.margin.bottom = 50;
		GUI.skin.button.fixedHeight = 50;

		selGridInt = GUILayout.SelectionGrid(selGridInt, selStrings, 4);

		if (selGridInt > -1) 
			Application.LoadLevel (selGridInt+1);


		GUILayout.EndArea ();
	}
}
