﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	private			ActionController actionControler;
	public	 		int					STATE;
	public	 		string	 			pState;

	public			SceneStage	 		MainScene;

	private			Vector2				forceJump;

	public	 		int					totalCoin;
	public	 		int					curCoin;

	public	 		Vector2				posMysteryBox;
	public	 		Vector2				forceMysteryBox;

	void Start (){
		actionControler = new ActionController();

		ChangeState (Define.STATE_CHAR_BEGIN);

		totalCoin = GameObject.FindGameObjectsWithTag ("Coin").Length;
	}

	public void Shoot(Vector2 force){

		forceJump = force;

		ChangeState (Define.STATE_CHAR_JUMP);
	}

	void Update (){
		if (STATE == Define.STATE_CHAR_IN_THE_GROUND) {
			StartCoroutine(PrepareEnd()) ;
		}

		actionControler.Update ();
	}




	#region ChangeState
	public void ChangeState(int state){
		actionControler.stopAllAction ();
		
		switch (state) {
			
		case Define.STATE_CHAR_BEGIN:
			actionControler.add(new Player_Action_Begin(gameObject));	

			pState = "STATE_CHAR_BEGIN";
			
			break;
			
		case Define.STATE_CHAR_JUMP:
			
			actionControler.add(new Player_Action_Jump(gameObject, forceJump));			
			
			pState = "STATE_CHAR_JUMP";
			
			break;	

		case Define.STATE_CHAR_IN_THE_GROUND:			
			rigidbody2D.drag = 1;

			MainScene.ChangeState(Define.STATE_SCENE_END);

			pState = "STATE_CHAR_IN_THE_GROUND";
			
			break;	

		case Define.STATE_CHAR_IN_MYSTERY_BOX:			
			Invoke("changeMysteryBox", 0.5f);
			
			pState = "STATE_CHAR_IN_MYSTERY_BOX";
			
			break;	

		case Define.STATE_CHAR_SHOOT_BY_MYSTERY_BOX:	
			Invoke("shootMysteryBox", 0.5f);

			transform.position = posMysteryBox;

			pState = "STATE_CHAR_SHOOT_BY_MYSTERY_BOX";

			break;	

		case Define.STATE_CHAR_JUMP_BY_MYSTERY_BOX:	

			
			pState = "STATE_CHAR_JUMP_BY_MYSTERY_BOX";
			
			break;	
		
		}
		
		
		STATE = state;		
	}
	
//	public void ChangeAnimation(int anim){
//		switch (anim) {
//			
//		case Define.ANIM_IDLE:
//			
//			animation.Play("Anim_Idle");
//			break;
//			
//		case Define.ANIM_JUMP:
//			
//			animation.Play("Anim_Jump");
//			break;
//			
//		case Define.ANIM_ROTATING:
//			
//			animation.Play("Anim_Rotate");		
//			break;
//			
//		case Define.ANIM_ROTATING_BALL:
//			animation.Play("Anim_Rotate_Ball");		
//			break;
//		}
//	}
	
	
	#endregion
	
	#region Colision

	void OnCollisionEnter2D(Collision2D coli) {
		if (coli.gameObject.tag == "Plane")
			ChangeState (Define.STATE_CHAR_IN_THE_GROUND);		

		if (coli.gameObject.tag == "Bounce") {
			coli.gameObject.tag = "Plane";
			Shoot(coli.transform.parent.GetComponentInChildren<Bounce>().force);
		}

	}

	void OnTriggerEnter2D(Collider2D coli) {
		if (coli.tag == "Coin") {
			curCoin++;
			GameObject.Destroy (coli.gameObject);
		}else if (coli.tag == "FireRing") {
			coli.transform.parent.GetComponentInChildren<FiringRing>().enabled = true;
		}else if (coli.tag == "MysteryBox") {
			ChangeState(Define.STATE_CHAR_IN_MYSTERY_BOX);

			posMysteryBox = coli.GetComponent<MysteryBox>().linkMysteryBox.position;
			forceMysteryBox = coli.GetComponent<MysteryBox>().force;

			transform.position = coli.transform.position;

			rigidbody2D.isKinematic = true;
		}
	}

	void OnTriggerExit2D(Collider2D coli) {
		if (coli.tag == "Bounce") {
			coli.isTrigger = false;
		}
	}
	#endregion

	#region waitAction
	IEnumerator PrepareEnd() {
		yield return new WaitForSeconds(2);
		if (MainScene.STATE != Define.STATE_SCENE_CHECK_RESULT) {
			Define.PERCENT_SCORE = curCoin*100 / totalCoin;
			
			MainScene.ChangeState (Define.STATE_SCENE_CHECK_RESULT);
		}
	}

	void changeMysteryBox(){
		ChangeState (Define.STATE_CHAR_SHOOT_BY_MYSTERY_BOX);
	}

	void shootMysteryBox(){
		Shoot(forceMysteryBox);

		ChangeState (Define.STATE_CHAR_JUMP_BY_MYSTERY_BOX);
	}

	#endregion
}
