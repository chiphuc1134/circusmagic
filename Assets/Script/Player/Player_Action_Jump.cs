using UnityEngine;
using System.Collections;

public class Player_Action_Jump : IActionPhuc {
	private			Player			playerComponent;


	public Player_Action_Jump (GameObject player, Vector2 force){
		this.player = player;	

		playerComponent = player.GetComponent<Player> ();

		playerComponent.rigidbody2D.isKinematic =  false; 
		playerComponent.rigidbody2D.AddForce(force);   
	}

	public override void Update () {
		     
	}	
}
