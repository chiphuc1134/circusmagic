﻿using UnityEngine;
using System.Collections;

public class Rectangle {

	// Create a rectangle in the center of screen
	public static Rect RectCenter(float w, float h)
	{
		return new Rect((Screen.width - w) / 2, (Screen.height - h) / 2, w, h);
	}

	// Get the width follow percent
	public static float PercentWidth(int p, int i)
	{
		return Screen.width/p * i;
	}

	// Get the height follow percent
	public static float PercentHeight(int p, int i)
	{
		return Screen.height/p * i;
	}
}
