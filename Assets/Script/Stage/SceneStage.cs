using UnityEngine;
using System.Collections;

public class SceneStage : MonoBehaviour {
	private 		GameObject			camera;
	private 		GameObject			player;
	public	 		GameObject			coins;


	private 		StageController		stageController;

	public	 		int					STATE;


	private			Texture				icon_Coin;
	private	 		Button 				b_Key_Pause;

	private	 		int					coin;
	private	 		bool				showCombo;
	private	 		float				delayTimeShowCombo;

	public GUISkin mygui;
	void Awake(){
		gameObject.AddComponent<Map>();

		camera = Camera.main.gameObject;

		player = GameObject.FindWithTag ("Player");

		float[,] map = Map.mapDict [Application.loadedLevelName];

		for (int i = 0; i < map.GetLength(0); i++) {
			Instantiate(coins, new Vector2(map[i,0], map[i,1]),Quaternion.identity);
		}
	}
	void Start () {
		stageController = new StageController ();

		b_Key_Pause = new Button ((Texture2D)Resources.Load ("StagePause/PauseButton"), new Rect(Screen.width - 50, 0, 50,50));

		icon_Coin = (Texture)Resources.Load ("Coin");

		ChangeState (Define.STATE_SCENE_IDLE);
	}	

	void Update () {

		stageController.Update ();

		if(Application.platform == RuntimePlatform.Android){
			ActionTouch ();
		}else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			ActionKey();
		}		
	}




	#region Action
	void ActionKey(){
		if (STATE == Define.STATE_SCENE_IDLE) {
			
			if (b_Key_Pause.isJustClick ()) {
				ChangeState (Define.STATE_SCENE_PAUSE);
			}
		}
	}

	void ActionTouch(){
		if (STATE == Define.STATE_SCENE_IDLE) {
			
			if (b_Key_Pause.isJustClick ()) {
				ChangeState (Define.STATE_SCENE_PAUSE);
			}
		}
	}
	#endregion

	#region ChangeState
	public void ChangeState(int state){
		stageController.stopAllStage ();

		switch (state) {

		case Define.STATE_SCENE_IDLE:			
			
			break;	

		case Define.STATE_SCENE_PAUSE:			

			stageController.add(new Scene_Stage_Pause());
			break;	

		case Define.STATE_SCENE_AIMING:	

			break;	

		case Define.STATE_SCENE_END:

			break;

		case Define.STATE_SCENE_CHECK_RESULT:
			stageController.add(new Scene_Stage_Result());

			break;
		}
		
		
		STATE = state;		
	}
	#endregion

	#region Draw
	void OnGUI(){
		b_Key_Pause.Draw ();

		stageController.Draw ();

		GUI.Label (new Rect (0, 0, 50, 50), icon_Coin);

		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUI.skin.label.normal.textColor = Color.black;
		GUI.skin.label.fontSize = 20;
		GUI.Label (new Rect (50, 0, 100, 50), coin.ToString());
	}
	#endregion

}
