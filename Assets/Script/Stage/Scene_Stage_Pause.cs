using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scene_Stage_Pause : IActionPhuc {

	Button resumeButton;
	Button replayButton;
	Button mainMenuButton;
	
	Rect rect = Utils.RectCenter(173, 190);

	private			Texture2D			imgPauseLayer;
	private			SceneStage			sceneStage;

	public Scene_Stage_Pause (){
		Time.timeScale = 0;
		
		Rect bigRect = Utils.RectCenter(rect, rect.width - 40, rect.height - 40);
		
		List<Rect> rects = Utils.SplitRectVertical(bigRect, 3, 10);
		
		resumeButton = new Button (Resources.Load<Texture2D>("StagePause/ResumeButton"), rects[0]);
		replayButton = new Button (Resources.Load<Texture2D>("StagePause/ReplayButton"), rects[1]);
		mainMenuButton = new Button (Resources.Load<Texture2D>("StagePause/MainMenuButton"), rects[2]);
		
		imgPauseLayer = (Texture2D) Resources.Load("StagePause/PauseLayer");

		sceneStage = GameObject.Find ("SceneStage").GetComponent<SceneStage> ();
	}

	public override void Draw () {
		GUI.DrawTexture(new Rect (0, 0, Screen.width, Screen.height), imgPauseLayer);
		
		GUI.DrawTexture(rect, Resources.Load<Texture2D>("StagePause/DialogBg"));	
		
		resumeButton.Draw();
		if(resumeButton.isJustClick())
		{
			Time.timeScale = 1;

			string newLevelName = "Map_0"+(Define.CUR_LEVEL++);
			print(newLevelName);
			Application.LoadLevel(newLevelName);
		}
		
		replayButton.Draw();
		if(replayButton.isJustClick())
		{
			Time.timeScale = 1;

			Application.LoadLevel(Application.loadedLevelName);
		}
		
		mainMenuButton.Draw();
		if (mainMenuButton.isJustClick ()) {
			Time.timeScale = 1;
			
			Application.LoadLevel("MainMenu");
		}
	}
}
