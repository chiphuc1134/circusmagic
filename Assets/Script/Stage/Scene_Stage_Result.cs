using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scene_Stage_Result : IActionPhuc {

	private			Button			bNextLevel;
	private			Button			bRestart;

	
	private			Texture2D		imgNextLevel;
	private			Texture2D		imgRestart;

	private			Texture2D		imgPauseLayer;
	private			Texture2D		imgBox;
	private			Texture2D[]		imgStar;

	private			Rect[]			rectStar;
	private			Rect			rectBox;
	private			Rect			rectScore;

	private			SceneStage		sceneStage;

	private			float 			h;

	private			int 			score;

	private			bool 			canCheckScore;

	public Scene_Stage_Result (){
		imgPauseLayer = (Texture2D) Resources.Load("StagePause/PauseLayer");

		imgStar = new Texture2D[3];
		imgStar[0] = (Texture2D) Resources.Load("StarBronze");
		imgStar[1] = (Texture2D) Resources.Load("StarSliver");
		imgStar[2] = (Texture2D) Resources.Load("StarGold");

		imgBox = (Texture2D) Resources.Load("StagePause/DialogBg");

		imgNextLevel = (Texture2D) Resources.Load("StagePause/ResumeButton");
		imgRestart = (Texture2D) Resources.Load("StagePause/ReplayButton");

		float w = Rectangle.PercentWidth (10, 3);
		h = Rectangle.PercentHeight(10,3);
		float xBox = Screen.width / 2 - w / 2;
		float yBox = (0 - h);
		rectBox = new Rect (xBox, yBox, w, h);

		rectStar = new Rect[3];
		float wStar = w / 4;
		float hStar = h / 3;
		float xStar = xBox;
		float yStar = yBox - hStar/2;
		rectStar [0] = new Rect (xStar, yStar, wStar, hStar);
	
		xStar = xBox + w/2;
		yStar = yBox - hStar/2;

		rectStar [1] = new Rect (xStar - wStar/2, yStar, wStar, hStar);


		xStar = xBox + w/2;
		yStar = yBox - hStar/2;
		rectStar [2] = new Rect (xStar + wStar, yStar, wStar, hStar);

		float wScore = w / 4;
		float hScore = h / 3;
		float xScore = Screen.width/2 - wScore/2;
		float yScore = yBox - hStar/2;
		rectScore = new Rect (xScore, yScore, wScore, hScore);

		float wButton = w / 2;
		float hButton = h / 5;
		float xButton = Screen.width/2 - wButton;
		float yButton = yBox + h - hButton;
		bNextLevel = new Button (imgNextLevel, new Rect (xButton + wButton*0.05f, yButton - hButton*0.05f, wButton * 0.9f, hButton * 0.9f));


		xButton = Screen.width/2;
		yButton = yBox + h - hButton;
		bRestart = new Button (imgRestart, new Rect (xButton + wButton*0.05f, yButton - hButton*0.05f, wButton * 0.9f, hButton * 0.9f));
	}

	public override void Update () {
		if (!canCheckScore) {
			rectBox.y += 500 * Time.deltaTime;
			
			if (rectBox.y >= Screen.height / 2 - h / 2) {
				rectBox.y = Screen.height / 2 - h / 2;
				canCheckScore = true;
			}
			
			rectStar [0].y = rectStar [1].y = rectStar [2].y = rectBox.y;
			rectScore.y = rectBox.y + rectStar [0].height;
			
			bRestart.rect.y = bNextLevel.rect.y = (rectBox.y + rectBox.height - bNextLevel.rect.height) - bNextLevel.rect.height * 0.1f;
		}else
			checkScore ();


		if (bNextLevel.isJustClick ()) {
			string newLevelName = "Map_0"+(++Define.CUR_LEVEL);
			print(newLevelName);

			Application.LoadLevel(newLevelName);
		}else if (bRestart.isJustClick ()) {
			Application.LoadLevel(Application.loadedLevelName);
		}


	}

	public override void Draw () {
		GUI.DrawTexture(new Rect (0, 0, Screen.width, Screen.height), imgPauseLayer);

		GUI.DrawTexture(rectBox, imgBox);

		if(score > 100 * 1 / 5)
		GUI.DrawTexture(rectStar[0], imgStar[0]);

		if(score > 100 * 2 / 5)
		GUI.DrawTexture(rectStar[1], imgStar[1]);

		if(score > 100 * 4 / 5)
		GUI.DrawTexture(rectStar[2], imgStar[2]);

		GUI.skin.label.normal.textColor = Color.black;
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		GUI.Label (rectScore, score.ToString());

		if (score == Define.PERCENT_SCORE) {
						bNextLevel.Draw ();
						bRestart.Draw ();
				}
	}

	public void checkScore(){
		if(score < Define.PERCENT_SCORE)
			score ++;
	}
}
